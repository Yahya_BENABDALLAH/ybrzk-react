import React, { useEffect } from "react";
import Son from "./wav/hero_simple-celebration-01.wav";

const Sound = () => {
  const playSound = async (audioFile) => {
    await audioFile.play();
  };
  const SonAudio = new Audio(Son);
  SonAudio.loop = false;
  useEffect(() => {
    playSound(SonAudio);
  }, []);

  return <div></div>;
};

export default Sound;
