import React from "react";
import "./style/style.css";
import { Jelly } from "react-rounder/Jelly";


const Loading = () => {
  return (
    <div className="outer">
      <Jelly size={100} color="#1026AA" />
    </div>
  );
};

export default Loading;
