import axios from "axios";

const NotorietyTime = "http://recrutement.notoriety-group.com/date.php";

const service = axios.create({
  baseURL: "http://127.0.0.1:8000/api",
  // delayed: true,
});
async function currTime() {
  var Time = await axios.get(NotorietyTime).then((e) => {
    return e;
  });
  return Time.data;
}
async function getUser(id) {
  return await service({
    method: "post",
    url: "/user/globalData?id=" + id,
  });
}
async function getUsers() {
  return await service({
    method: "post",
    url: "/user/getAdminData",
  });
}
function demandeP(id, gId) {
  return service({
    method: "post",
    url: "/user/demandePause?userID=" + id + "&groupID=" + gId,
  });
}
function accepterP(id, gId) {
  return service({
    method: "post",
    url: "/user/startPause?userID=" + id + "&groupID=" + gId,
  });
}
function arreterP(id, gId) {
  return service({
    method: "post",
    url: "/user/finichPause?userID=" + id + "&groupID=" + gId,
  });
}
function resetP(gId) {
  return service({
    method: "post",
    url: "/user/resetPauses?groupID=" + gId,
  });
}
function depasserP(id, gId) {
  return service({
    method: "post",
    url: "/user/passPause?userID=" + id + "&groupID=" + gId,
  });
}
function updateMax(id, gId, max) {
  return service({
    method: "post",
    url: "/user/updateMax?userID=" + id + "&groupID=" + gId + "&max=" + max,
  });
}
function updateEtat(isOpen, gId) {
  return service({
    method: "post",
    url: "/user/updateEtat?groupID=" + gId + "&isOpen=" + isOpen,
  });
}
function getRank(isOpen, gId) {
  return service({
    method: "post",
    url: "/user/updateEtat?groupID=" + gId + "&isOpen=" + isOpen,
  });
}
function dechifree(id) {
  return id;
}
function awaitUser(id) {
  return service({
    method: "post",
    url: "/user/awaitPause?userID=" + id,
  });
}
async function UserSession() {
  return await axios({
    method: "get",
    url: "http://192.168.1.150/sessionForBreaktool",
    responseType: "stream",
  });
}
async function getLate(id) {
  return await service({
    method: "post",
    url: "/user/globalData?id=" + id,
  });
}
export {
  UserSession,
  currTime,
  getUser,
  demandeP,
  accepterP,
  arreterP,
  resetP,
  depasserP,
  updateMax,
  updateEtat,
  getUsers,
  dechifree,
  getRank,
  awaitUser,
  getLate,
};
