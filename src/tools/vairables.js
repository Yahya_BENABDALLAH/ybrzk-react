const TimeDepassement = 680; /// 15 min depassement
const RefreshUser = 5000; /// Temps refresh pour user
const RefreshAdmin = 5000;/// Temps refresh pour Admin
const NotifDelay = 30;/// Temps pour depasser la pause
const NotifAwait = 120;/// Temps pour depasser la pause
const ChronoRefresh = 1000; /// Temps refresh chrono
const NotifChronoCheck = 1000; /// Temps refresh pour veriier temps notif


export {
  TimeDepassement,
  RefreshUser,
  RefreshAdmin,
  NotifDelay,
  ChronoRefresh,
  NotifAwait,
  NotifChronoCheck,
};