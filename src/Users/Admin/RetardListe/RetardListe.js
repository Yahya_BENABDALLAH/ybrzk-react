import React, { useEffect, useState } from "react";
import MaterialTable from "material-table";
import {TimeDepassement} from "../../../../src/tools/vairables"
import "./style/style.css";

const RetardListe = ({ user}) => {
  const [data, setdata] = useState([]);
  const [Title, setTitle] = useState("");

  const secToMin = (Diff) => {
    var Minutes = Math.floor(Diff / 60);
    var Seconds = Diff % 60;
    return (
      "00:" +
      (Minutes < 10 ? "0" : "") + Minutes +
      ":" +
      (Seconds < 10 ? "0" : "") +
      Seconds
    );
  };

  var fixdata = (data) => {
    setTitle(data.group.label)
    return data.pauses
    .filter((e) => e.etat === 2 && (e.dateFin - e.datePause)>TimeDepassement)
      .map((e) => {
              var dif = e.dateFin - e.datePause;
              var dateDebut = new Date(e.datePause * 1000)
                .toString()
                .substring(16, 24);
              var dateFini = new Date(e.dateFin * 1000)
                .toString()
                .substring(16, 24);
              var difMinute = secToMin(dif);
        return {
          ...e,
          dateDebut,
          dateFini,
          difMinute,
          diff: secToMin(dif - TimeDepassement)
        };
      });
  };

    useEffect(() => {
      if (user !== 0 ) { 
        var retards = fixdata(user);
        setdata(retards);
      }
      
    }, []);

  return (
    <div className="tableRetard">
      <MaterialTable
        title="Dépassement"
        data={data}
        columns={[
          {
            title: "Pseudo",
            field: "label",
            cellStyle: {
              textAlign: "center",
            },
          },
          {
            title: "Dèbut pause",
            field: "dateDebut",
            cellStyle: {
              textAlign: "center",
            },
          },
          {
            title: "Fin pause",
            field: "dateFini",
            cellStyle: {
              textAlign: "center",
            },
          },
          {
            title: "Dépassement",
            field: "diff",
            cellStyle: {
              textAlign: "center",
            },
          },
          {
            title: "Temps",
            field: "difMinute",
            cellStyle: {
              textAlign: "center",
            },
          },
        ]}
        localization={{
          body: {
            emptyDataSourceMessage: "Aucun dépassement détecter.",
          },
          toolbar: {
            exportTitle: "Enregistrer",
          },
        }}
        style={{
          width: "100%",
          tableLayout: "fixed",
          boxShadow: "0px 0px 15px 6px rgba(0,0,0,0.1)",
          borderRadius: "20px",
        }}
        options={{
          draggable: false,
          headerStyle: {
            backgroundColor: "#154c79",
            color: "#ffffff",
            textAlign: "center",
            fontWeight: "bold",
            width: "22px",
            position: "sticky",
          },
          maxBodyHeight: "300px",
          minBodyHeight: "300px",
          search: false,
          paging: false,
          exportAllData: false, /// a revoir
          exportButton: true,
          exportFileName:
            "Listes des dépassement :" +
            Title +
            " - " +
            new Date(Date.now()).toString().substring(0, 24),
        }}
        actions={[]}
      />
    </div>
  );
};

export default RetardListe;
