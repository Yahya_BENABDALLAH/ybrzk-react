import React, {  useEffect } from "react";
import { resetP, updateMax, updateEtat } from "../../../tools/axios";
import { confirmAlert } from "react-confirm-alert";
import "react-confirm-alert/src/react-confirm-alert.css";
import "./style.css";

export const AdminCard = ({ data, users, refrech }) => {
  const maxi = users.group.max

  useEffect(() => {
    return () => {};
  }, [data, users, refrech]);

  const  miseMax = ()=>{
            updateMax(
              data.user.id,
              users.group.id,
              document.querySelector(".Max-input").value
            ).then((e) => {
              refrech(data.user.id);
            });
  }

  const Reset = () => {
    confirmAlert({
      title: "Reset",
      message:
        "Êtes-vous sûr de vouloir établir un Reset totale des pauses du groupe " +
        users.group.label +
        " ?",
      buttons: [
        {
          label: "Oui",
          onClick: () =>
            resetP(users.group.id).then((e) => {
              refrech(data.user.id);
            }),
        },
        {
          label: "Non",
          onClick: () => refrech(data.user.id),
        },
      ],
      overlayClassName: "AlertReset",
    });
  };

  const UpdateMax = () => {
    confirmAlert({
      title: "Changement du Maximum",
      message: "Êtes-vous sûr de changer le nombre maximum des pauses ?",
      buttons: [
        {
          label: "Oui",
          onClick: () => {
            miseMax();
          },
        },
        {
          label: "Non",
          onClick: () => refrech(data.user.id),
        },
      ],
      childrenElement: () => (
        <input
          type="Number"
          className="Max-input"
          placeholder="Insérer le nouveau Max !"
          min="1"
        ></input>
      ),
      overlayClassName: "AlertMax",
    });
  };

  const isOpen = () => {
    confirmAlert({
      title: "Etat du groupe " + users.group.label,
      message:
        "Confirmer-vous " +
        (users.group.isOpen === 1 ? "la fermeture " : "l'ouverture ") +
        " des pauses ?",
      buttons: [
        {
          label: "Oui",
          onClick: () => {
            if (users.group.isOpen === 1) {
              updateEtat(0, users.group.id).then((e) => {
                refrech(data.user.id);
              });
            } else {
              updateEtat(1, users.group.id).then((e) => {
                refrech(data.user.id);
              });
            }
          },
        },
        {
          label: "Non",
          onClick: () => refrech(data.user.id),
        },
      ],
      overlayClassName: "AlertEtat",
    });
  };

  return (
    <div className="adminContainer">
      <p className="Max-h2">
        <strong>Max : {maxi} </strong>
      </p>
      <button className="btnMax" onClick={UpdateMax}>
        Mise à jour du Max
      </button>
      <button className="btnOnOff" onClick={isOpen}>
        {users.group.isOpen === 1 ? "Fermer" : "Ouvrir "}
      </button>
      <button className="btnReset" onClick={Reset}>
        Reset
      </button>
    </div>
  );
};
