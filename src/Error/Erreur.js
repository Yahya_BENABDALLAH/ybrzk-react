import React from "react";
import "./style/styleError.css";

const Erreur = () => {
  return (
    <div>
      <div className="Error-container">
        <div className="Error-txt-img"></div>
        <div className="Error-notoriety"></div>
        <div className="Error-img"></div>
        <div className="Error-Message">
          <h2 className="Error-h2">Oops! la page demandée n'existe pas</h2>
          <p className="Error-p">
            Désole, vous n'êtes pas encore inscrit dans la base de données,
            <br></br>
            votre ID est modifié ou bien le service est désactivé temporairement
          </p>
        </div>
      </div>
    </div>
  );
};

export default Erreur;
