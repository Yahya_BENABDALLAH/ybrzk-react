import React, { useEffect, useState } from "react";
import { NotifAwait, NotifChronoCheck, NotifDelay } from "../tools/vairables";
import Sound from "../Sounds/Sound";
import { accepterP, awaitUser, depasserP } from "../tools/axios";
import "./style.css";

const Notifs = ({ user, refrech }) => {
  const [data, setdata] = useState(0);
  const [disabled, setDisabled] = useState(false);

  ////////// Gestion affichage button//////////////
  ////////////////////////////////////////////////
  const btnPatienter=
    user.pauses.filter((e) => e.userID === user.user.id && e.awaiting === 1)
      .length


  const accepterPause = () => {
    //update etat pause =>
    //update datePause
    //notif off
    setDisabled(true);
    accepterP(user.user.id, user.user.compagne).then((e) => {
      refrech(user.user.id);
    });
  };

  const depasserPause = () => {
    //update ranking
    //update notif
    //update notifdate
      setDisabled(true);
      depasserP(user.user.id, user.user.compagne).then((e) => {
        refrech(user.user.id);
      });
 
  };
  const patienterPause = () => {
    setDisabled(true);
    awaitUser(user.user.id).then((e) => {
      refrech(user.user.id);
    });
  };

  const secToMin = (Diff) => {
    var Minutes = Math.floor(Diff / 60);
    var Seconds = Diff % 60;
    return Minutes + ":" + (Seconds < 10 ? "0" : "") + Seconds;
  };

  const addSec = (data) => {
    var temp = data;
    return {
      ...temp,
      diff: temp.diff + 1,
      Chrono: secToMin(temp.diff + 1),
    };
  };

  var fixdata = (data) => {
    var dif = user.time - data.user.notifDate;
    return { ...data.user, Chrono: secToMin(dif), diff: dif };
  };

  useEffect(() => {
    if (user !== 0) {
      user.user = fixdata(user);
      setdata(user.user);
      var timer = setInterval(() => {
        user.user = addSec(user.user);
        setdata(user.user);
        if (btnPatienter === 0) {
          if (user.user.diff >= NotifDelay) {
            depasserPause();
          }
        } else {
          if (user.user.diff >= NotifAwait) depasserPause();
        }
      }, NotifChronoCheck);
      
      return () => {
        clearInterval(timer);
      };
    }
  }, [user]);

  return (
    <>
      <div className="notifContainer">
        {user.user.diff<=5 ? <Sound/>:""}
        <div className="notifHeader">
          <p>Voulez-vous partir en pause ?</p>
          <p>{data.Chrono}</p>
          <div className="notifBtn">
            <button
              className="btnAccepter"
              disabled={disabled}
              onClick={accepterPause}
            >
              Accepter
            </button>
            <button
              className="btnDepasser"
              disabled={disabled}
              onClick={depasserPause}
            >
              Depasser
            </button>
            {btnPatienter === 0 ? (
              <>
                <button
                  className="btnPatienter"
                  disabled={disabled}
                  onClick={patienterPause}
                >
                  Patienter 2:00 min
                </button>
              </>
            ) : (
              ""
            )}
          </div>
        </div>
      </div>
    </>
  );
};

export default Notifs;
